<?php

 //連接資料庫
 $link = @mysqli_connect("127.0.0.1:3306", "root", "");
 if (!$link) {
   // Connect successfully
 } else {
  // Connect fail
 }


 //使用者如果已經登入則轉至chat.php



 //檢查使用者登入帳號與密碼並設定對應錯誤訊息


 $db = mysqli_select_db($link, "livechat");
 mysqli_query($link, "SET NAMES 'utf8'"); // 設定中文



?>
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <title>登入</title>  
 <link rel="stylesheet" href="css/bootstrap.min.css">
 <script src="js/jquery-3.4.1.min.js"></script>
</head>  
<body>  
 <div class="container">
   <h2 class="text-center text-primary">即時聊天系統-Live Chat</h2>
   <div class="row">
    <div class="col-md-4" style="margin:0 auto">
   <div class="card border-0">
     <form method="post">    
      <div class="form-group">
       <label class="text-success">帳號</label>
       <input type="text" name="uaccount" class="form-control" required />
      </div>
      <div class="form-group">
       <label class="text-success">密碼</label>
       <input type="password" name="password" class="form-control" required />
      </div>
      <div class="form-group">
       <input type="submit" name="login" class="btn btn-info" value="登入" />
      </div>
     </form>
     <p class="text-danger">
	 	<?php
     //顯示登入錯誤訊息
     // Check if account & password are empty
     if (!empty($_POST['uaccount']) && !empty($_POST['password'])) {
      $input_username = $_POST['uaccount'];
      $input_password = $_POST['password'];

      $query_result = mysqli_query($link, "select * from `account` where uaccount='$input_username'");

      $isCorrect = FALSE;

      // Get per row from query result
      while($row = mysqli_fetch_row($query_result)) {
        if ($row[3] == $input_password) {

          $profile = ['uid' => $row[0], 'uname' => $row[1]]; // Get profile from database
          
          // Save profile to session
          session_start();

          $_SESSION['uid'] = $row[0];
          $_SESSION['uname'] = $row[1];

          // When login, update the `login` table in database
          mysqli_query($link, "insert into login(`uid`, `activetime`) values ($row[0], NOW())");
          

          // header("LOCATION: chat.php"); // Redirect page to chat.php
          $isCorrect = TRUE;
          break;
        }
      }

      if ($isCorrect == FALSE) {
        echo "帳號錯誤";
      }
     }
		?>
     </p>
  </div>
  </div>
  </div>
    </body>  
</html>