setInterval(() => {

    // Time
    let date = new Date();
    const hour = date.getHours();
    const minute = date.getMinutes();
    const second = date.getSeconds();

    const canva = document.getElementById('clock');
    const ctx = canva.getContext("2d");

    // Outer circle
    ctx.lineWidth = 20;
    ctx.strokeStyle = 'orange';
    ctx.fillStyle = 'yellow';
    ctx.beginPath();
    ctx.arc(250, 250, 240, 0, Math.PI * 2, false);
    ctx.stroke();
    ctx.fill()
    ctx.closePath();

    ctx.beginPath();
    ctx.font = '36px Consolas';
    ctx.fillStyle = 'red';
    ctx.fillText('12', 230, 50);
    ctx.fillText('1', 350, 75);
    ctx.fillText('2', 425, 145);
    ctx.fillText('3', 460, 260);
    ctx.fillText('4', 425, 380);
    ctx.fillText('5', 350, 450);
    ctx.fillText('6', 240, 480);
    ctx.fillText('7', 130, 450);
    ctx.fillText('8', 50, 380);
    ctx.fillText('9', 20, 260);
    ctx.fillText('10', 50, 145);
    ctx.fillText('11', 130, 75);
    ctx.closePath();

    ctx.fillStyle = 'black';
    ctx.fillText(hour + ':' + minute + ':' + second, 175, 150)


    // Hour hand
    ctx.beginPath();
    ctx.lineWidth = 12;
    ctx.strokeStyle = 'black';
    ctx.moveTo(250, 250);
    ctx.lineTo(250 + 120 * Math.sin(2 * Math.PI * (hour + minute / 60) / 12), 250 - 120 * Math.cos(2 * Math.PI * (hour + minute / 60) / 12));
    ctx.stroke();
    ctx.closePath();

    // Minute hand
    ctx.beginPath();
    ctx.lineWidth = 8;
    ctx.strokeStyle = 'black';
    ctx.moveTo(250, 250);
    ctx.lineTo(250 + 170 * Math.sin(2 * Math.PI * minute / 60), 250 - 170 * Math.cos(2 * Math.PI * minute / 60));
    ctx.stroke();
    ctx.closePath();

    // Inner1 circle
    ctx.lineWidth = 0.1;
    ctx.fillStyle = 'blue';
    ctx.strokeStyle = 'blue';
    ctx.beginPath();
    ctx.arc(250, 250, 12, 0, Math.PI * 2, false);
    ctx.stroke();
    ctx.fill();
    ctx.closePath();

    // Inner2 circle
    ctx.fillStyle = 'red';
    ctx.strokeStyle = 'red';
    ctx.beginPath();
    ctx.arc(250, 250, 5, 0, Math.PI * 2, false);
    ctx.stroke();
    ctx.fill();
    ctx.closePath();


    // Second Hand
    ctx.beginPath();
    ctx.lineWidth = 5;
    ctx.strokeStyle = 'red';
    ctx.moveTo(250, 250);
    ctx.lineTo(250 + 200 * Math.sin(2 * Math.PI * second / 60), 250 - 200 * Math.cos(2 * Math.PI * second / 60));
    ctx.stroke();
    ctx.closePath();
}, 1000);
